# automatasI

Programas de la materia Lenguajes y Automatas I

## Requisitos
- Tener instalado el JDK/OpenJDk v. 11 o mayor.
- Tener instalado Apache Maven.

## Descargar proyecto

``` sh
git clone https://gitlab.com/mugcake/automatasi.git
```

## Ejecutar

### Recomendado (Automatizado)
Abrir la consola del sistema (terminal) en la carpeta del proyecto y ejecutar:

``` sh
bash run.sh
```

### Manual

Abrir la consola del sistema (terminal) en la carpeta del proyecto y ejecutar:

``` sh
java src/main/java/com/alexius/app/[archivo].java 
```

Cambia [archivo] por el nombre del programa que Quieres ejecutar, por ejemplo: Trabajo1
