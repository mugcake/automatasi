package com.alexius.app.unidad1;

public class Sufijo {
   private static int capacidad;
   
   public void imprimirSufijo(String cadena){
      String aux = "";
      String aux1 = "";
      capacidad = cadena.length();

      System.out.println("----- SUFIJO");
      for (int i = capacidad - 1 ; i >= 0; i--){
         char a = cadena.charAt(i);
         aux = Character.toString(a);
         aux1 = aux + aux1;
         
         if (capacidad - 1 != i)
            System.out.printf("%s\n ", aux1);
         else if (capacidad - 1 == i)
            System.out.printf(" %s\n ", aux);
      }
      System.out.println("\n");  
   }
   
}
