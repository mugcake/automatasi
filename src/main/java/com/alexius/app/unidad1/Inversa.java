package com.alexius.app.unidad1;

public class Inversa {
   private static int capacidad;
   
   public void imprimirInversa(String cadena){
      String aux = "";
      String aux1 = "";
      String aux2="" ;
      capacidad = cadena.length();

      System.out.println("----- INVERSA");
      for (int i = 0; i < capacidad; i++){
         char a = cadena.charAt(i);
         aux =  Character.toString(a);
         aux1 = aux + aux1;
         
         for (int j = i+1; j< capacidad; j++){
            char b = cadena.charAt(j);
            aux =  Character.toString(b);
            aux2 += aux;
         }
         
         if (capacidad != i)
            System.out.printf("(%s) %s\n ",aux2, aux1);
         else if (capacidad == 0)
            System.out.printf("(%s) %s\n ",aux2, aux);
         aux = "";
         aux2 ="";
      }
      System.out.println("\n");
   }
   
}
