/*
 * =================================
 *     Lenguajes y automatas I
 * =================================
 * Profesor: Alejandro "alexius"
 *
 * Unidad 1
 */
package com.alexius.app.unidad1;

import java.util.Scanner;

public class Principal{
      private static Scanner leer;
      private static Prefijo pre;
      private static Sufijo suf;
      private static Subcadena sub;
      private static Inversa inv;

   public Principal (){
      leer = new Scanner(System.in);
      pre = new Prefijo();
      suf = new Sufijo();
      sub = new Subcadena();
      inv = new Inversa();
      
      String cadena;

      System.out.println();
      System.out.println("------------------------------");
      System.out.println("1.  OPERACIONES CON CADENAS ");
      System.out.println("------------------------------");
      System.out.println();
      System.out.print("Ingrese una cadena: ");
      cadena = leer.nextLine();
      leer.close();
      System.out.println();
      
      pre.imprimirPrefijo(cadena);
      suf.imprimirSufijo(cadena);
      sub.impimirSubcadena(cadena);
      inv.imprimirInversa(cadena);
   }
}
